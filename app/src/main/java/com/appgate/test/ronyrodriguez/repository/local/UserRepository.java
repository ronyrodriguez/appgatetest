package com.appgate.test.ronyrodriguez.repository.local;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.appgate.test.ronyrodriguez.datasource.local.AppDatabase;
import com.appgate.test.ronyrodriguez.datasource.local.User;
import com.appgate.test.ronyrodriguez.datasource.local.UserDao;

public class UserRepository {

    private UserDao userDao;
    private LiveData<User> userLiveData;

    public UserRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        userDao = db.userDao();
    }

    public LiveData<User> login(String username, String password) {
        return userDao.login(username, password);
    }

    public LiveData<User> exists(String username) {
        return userDao.exists(username);
    }

    public void insert(User user) {
        new insertAsyncTask(userDao).execute(user);
    }

    private static class insertAsyncTask extends AsyncTask<User, Void, Void> {

        private UserDao userDao;

        insertAsyncTask(UserDao dao) {
            userDao = dao;
        }

        @Override
        protected Void doInBackground(final User... params) {
            userDao.insert(params[0]);
            return null;
        }
    }

}
